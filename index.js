const utils = require("utils");
const axios = require('axios');
const htmlToText = require('html-to-text');
const request = require('request-promise');
const fetch = require('node-fetch');
const AbortController = require('abort-controller');



exports.test = function() {
    utils.log('test');
}

exports.syncPostForm = async function(url, data, headers = [], timeout = 0) {

    headers['Content-Type'] = 'application/x-www-form-urlencoded';

    var data = utils.jsonToQueryString(data);

    return await exports.syncRequest('post', url, data, headers, timeout);
};

exports.syncPost = async function(url, data, headers = [], timeout = 0) {
    return await exports.syncRequest('post', url, data, headers, timeout);
};

exports.syncGet = async function(url, data, headers = [], timeout = 0) {
    return await exports.syncRequest('get', url, data, headers, timeout);
};

exports.asyncPost = function(url, data, onSuccess, headers = [], timeout = 0) {
    return exports.asyncRequest('post', url, data, onSuccess, headers, timeout);
};

exports.asyncGet = function(url, data, onSuccess, headers = [], timeout = 0) {
    return exports.asyncRequest('get', url, data, onSuccess, headers, timeout);
};

exports.requestErrorHandler = function(error) {
    if (process.env.APPLICATION_ENV == 'production') return;
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        utils.log('error.response');
        utils.log(error.response.config.url);
        utils.log(error.response.config.data);
        console.log(htmlToText.fromString(error.response.data));
        //log(error.response.data);
        //log(error.response.status);
        //utils.log(error.response);
        console.log('Error', error.message, 'HTTP' + error.response.status);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        utils.log('error.request');
        //log(error.request);
        console.log('Error', error.message, error.code + ' ' + error.errno);
    } else {
        // Something happened in setting up the request that triggered an Error
        utils.log('error');
        console.log('Error', error.message);
    }
    //console.log(error);
};

exports.syncRequest = async function(method, url, data, headers, timeout = 0) {
    data = (utils.isEmpty(data) ? null : data);
    headers = (utils.isEmpty(headers) ? null : headers);
    let response = {};

    try {
        response.data = (await axios({ method: method, url: url, data: data, headers: headers, timeout: timeout })).data;
        response.error = null;
    } catch (error) {
        exports.requestErrorHandler(error);
        response.data = { status: 0 }
        htmlToText.fromString(error)
        response.error = error.toString();
    }

    return response;
};

exports.asyncRequest = function(method, url, data, callback, headers, timeout = 0) {
    data = (utils.isEmpty(data) ? null : data);
    headers = (utils.isEmpty(headers) ? null : headers);
    callback = (typeof callback === 'function' ? callback : function() {});
    try {
        axios({ method: method, url: url, data: data, headers: headers, timeout: timeout })
            .then(function(response) {
                callback({ data: response.data, error: null });
            })
            .catch(function(error) {
                exports.requestErrorHandler(error);
                callback({ data: { status: 0 }, error: error.toString() });
            });
    } catch (error) {
        callback({ data: { status: 0 }, error: error.toString() });
    }
};

exports.formPost = async function(url, data) {
    try {
        const r = await request({method: 'POST', uri: url, form: data});
        return r;
    } catch (e) {
        utils.error(e.message);
    }
}

exports.get = async function(url) {
    let r = "";

    try {
        r = await request({method: 'GET', uri: url});
    } catch (e) {
        utils.error('requestHelper.get', e.message);
    } finally {
        return r;
    }
}

exports.getJSON = function(url, timeout = 0) {
    let aController = new AbortController();

    return new Promise(async function (resolve, reject) {
        let result = null;

        const abortTimer = setTimeout(() => {
            aController.abort();
            resolve(result);
        }, timeout);

        try {
            result = (await fetch(url, {
                method: 'GET',
                signal: aController.signal
            })).json();
        } catch (e) {

        } finally {
            clearTimeout(abortTimer);
            resolve(result);
        }
    });
}

exports.postJSON = function(url, data, timeout = 0) {
    let aController = new AbortController();

    return new Promise(async function (resolve, reject) {
        let result = null;

        const abortTimer = setTimeout(() => {
            aController.abort();
            resolve(result);
        }, timeout);

        try {
            let response = (await fetch(url, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data),
                signal: aController.signal
            }));

            //utils.log('response.status', response.status);
            if (response.status == 200)
                result = response.json();
        } catch(e) {
        } finally {
            clearTimeout(abortTimer);
            resolve(result);
        }
/**/
        /*
        fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify(data),
            signal: aController.signal
        }).then((res) => {result = res.json()})
            .catch((e) => {
                reject({});
            })
            .finally(() => {
                if (result != null) {
                    resolve(result)
                } else {
                    reject({});
                }
                clearTimeout(abortTimer);
            });
/**/
    });
}

/*

function asyncPost(url, request) {
    request = (utils.isEmpty(request) ? null : request);
    log('asyncPost call : ' + url);
    log('asyncPost request');
    log(request);

    let response = {r:0};
    try {
        axios.post(url, request).then(function (response) {
            log('asyncPost response');
            return response
        }).catch(function (error) {
            htmlToText.fromString(error)
        });
    } catch (error) {
        handleError(error);
    }
    //log('post response');
    //log((utils.isEmpty(response) ? {} : response));
    log('asyncPost end');
}

function asyncGet(url, request) {
    request = (utils.isEmpty(request) ? null : request);
    log('asyncGet call : ' + url);
    log('asyncGet request');
    log(request);

    try {
        axios.get(url, request).then(function (response) {
            log('asyncGet response');
            return response
        }).catch(function (error) {
            htmlToText.fromString(error)
        });
    } catch (error) {
        handleError(error);
    }
    return {r:0};
    //log('post response');
    //log((utils.isEmpty(response) ? {} : response));
    log('asyncGet end');
}

async function syncPost(url, request) {
    request = (utils.isEmpty(request) ? null : request);
    log('syncPost call : ' + url);
    log('syncPost request');
    log(request);

    let response = {r:0};
    try {
        response = await axios.post(url, request).then(function (response) {
            return response;
        }).catch(function (error) {
            htmlToText.fromString(error.response)
        });
    } catch (error) {
        handleError(error);
    }
    //log('post response');
    //log((utils.isEmpty(response) ? {} : response));
    log('syncPost end');
    return response;
}

async function syncGet(url, request) {
    request = (utils.isEmpty(request) ? null : request);
    log('get call : ' + url);
    log('get request');
    log(request);

    let response = {r:0};
    try {
        response = await axios.get(url, request).then(function (response) {
            return response
        }).catch(function (error) {
            htmlToText.fromString(error.response)
        });
    } catch (error) {
        handleError(error);
    }
    //log('get response');
    //log((utils.isEmpty(response.data) ? {} : response.data));
    log('get end');
    return response;
}

*/
